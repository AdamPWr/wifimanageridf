#include"httpServer.hpp"
#include<esp_log.h>

static const char *TAG = "HTTPServer";

static esp_err_t get_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG,"GET");
    /* Send a simple response */
    char* resp_str = "<html><body><h1>Login Page</h1><form><label for='username'>Username:</label><input type='text' id='username' name='username'><br><label for='password'>Password:</label><input type='password' id='password' name='password'><br><input type='submit' value='Login'></form></body></html>";
    httpd_resp_send(req, resp_str, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

HttpServer::HttpServer()
{
    uri_get = {};
    uri_get.uri = "/";
    uri_get.method = HTTP_GET;
    uri_get.user_ctx = nullptr;
    uri_get.handler = get_handler;

}

HttpServer::~HttpServer()
{
    stopServer();
}

bool HttpServer::startServer()
{

    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.server_port = 80;

    /* Empty handle to esp_http_server */
    server = NULL;

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get);
        // httpd_register_uri_handler(server, &uri_post);
    }
    /* If server failed to start, handle will be NULL */
    return server == nullptr ? false:true;

}

void HttpServer::stopServer()
{
    if (server)
        {
            httpd_stop(server);
        }
}