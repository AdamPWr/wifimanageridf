#include"wifiManagerIdf.h"
#include<stdio.h>
#include <cstring>
#include<esp_log.h>
#include<memory>
#include "esp_netif.h"
#include "esp_wifi.h"
#include "lwip/ip_addr.h"
#include"dnsRedirector.h"




static const char *TAG = "wifi softAP";

WifiManagerIdf::WifiManagerIdf()
{
    startAP();
    bool serverStarted = startHttpServer();
    if(serverStarted)
    {
        ESP_LOGI(TAG,"[DEBUG] Server started");
    }
    else
    {
        ESP_LOGI(TAG,"[DEBUG] Server NOT started");
    }

    start_dns_server();
}

void set_wifi_ap_ip() 
{
    esp_netif_t *esp_netif_ap = esp_netif_create_default_wifi_ap();
    
    esp_netif_ip_info_t IP_settings_ap;

    IP_settings_ap.ip.addr=ipaddr_addr("192.168.4.1");
    IP_settings_ap.netmask.addr=ipaddr_addr("225.255.255.0");
    IP_settings_ap.gw.addr=ipaddr_addr("192.168.4.1");
    ESP_ERROR_CHECK(esp_netif_dhcps_stop(esp_netif_ap));
    // ESP_ERROR_CHECK(esp_netif_set_ip_info(esp_netif_ap, &IP_settings_ap));
    ESP_ERROR_CHECK(esp_netif_dhcps_start(esp_netif_ap));

}

static void wifiEventHandler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    auto wifiManagerIdf = static_cast<WifiManagerIdf*>(arg);

    if(!wifiManagerIdf)
    {
        ESP_LOGD(TAG,"[DEBUG]casting is not ok");
    }
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG,"AP connected");
        // ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
        //          MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG,"AP disconected");
        // ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
        //          MAC2STR(event->mac), event->aid);
    }
}

void WifiManagerIdf::startAP()
{

    const char* ssid = "Legia Pany";
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    set_wifi_ap_ip();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifiEventHandler,
                                                        this,
                                                        NULL));
   wifi_config_t ap_config = {};
    strncpy((char*)ap_config.ap.ssid, ssid, sizeof(ap_config.ap.ssid) - 1);
    ap_config.ap.ssid_len = strlen(ssid);
    // strncpy((char*)ap_config.ap.password, password, sizeof(ap_config.ap.password) - 1);
    ap_config.ap.max_connection = 1;
    ap_config.ap.authmode = WIFI_AUTH_OPEN;

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &ap_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
             ssid, "no pasword", 3);
}

bool WifiManagerIdf::startHttpServer()
{
    if(httpServer_ptr)
    {
        ESP_LOGD(TAG,"HttpServer already started");
        return false;
    }

    httpServer_ptr = std::make_unique<HttpServer>();
    return httpServer_ptr->startServer();
}

void WifiManagerIdf::stopHttpServer()
{
    if(httpServer_ptr == nullptr)
    {
        ESP_LOGE(TAG,"HttpServer already stopped");
    }
    httpServer_ptr.reset(); // it calls HttpServer destructor 

}