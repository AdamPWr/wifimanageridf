#pragma once
#include <esp_http_server.h>

class HttpServer
{
    httpd_handle_t server;
    httpd_uri_t uri_get;

    // esp_err_t get_handler(httpd_req_t *req);

public:
    HttpServer();
    ~HttpServer();
    bool startServer(); 
    void stopServer();  // TODO consider moving it to private scope
};