#pragma once
#include<memory>
#include"httpServer.hpp"
#include "esp_wifi.h"

class WifiManagerIdf
{

    std::unique_ptr<HttpServer> httpServer_ptr;

    void startAP();
    bool startHttpServer();
    void stopHttpServer();

public:
    WifiManagerIdf();

};